locals {
  health_checks = concat(google_compute_health_check.http[*].self_link, google_compute_health_check.tcp[*].self_link)
  os_disk_create_nodes = {
    for index, node in var.nodes : index => node
    if node.os_disk_snapshot != ""
  }
}

resource "google_compute_address" "external" {
  for_each = var.use_external_ip ? var.nodes : {}

  name = format("%v-%02d-%v-%v-static-ip",
    var.name,
    each.key,
    var.tier,
    var.environment,
  )
  address_type = "EXTERNAL"
}

resource "google_compute_region_backend_service" "default" {
  count = var.backend_service_type == "regional" && var.create_backend_service ? 1 : 0

  name     = format("%v-%v-regional", var.environment, var.name)
  protocol = "TCP"

  backend {
    balancing_mode = var.backend_balancing_mode
    group          = google_compute_instance_group.default[0].self_link
  }

  backend {
    balancing_mode = var.backend_balancing_mode
    group          = google_compute_instance_group.default[1].self_link
  }

  backend {
    balancing_mode = var.backend_balancing_mode
    group          = google_compute_instance_group.default[2].self_link
  }

  connection_draining_timeout_sec = var.backend_connection_draining_timeout

  health_checks = local.health_checks
}

resource "google_compute_backend_service" "default" {
  count = var.backend_service_type != "regional" && var.create_backend_service ? 1 : 0

  name      = format("%v-%v", var.environment, var.name)
  protocol  = var.backend_protocol
  port_name = var.name

  backend {
    balancing_mode = var.backend_balancing_mode
    group          = google_compute_instance_group.default[0].self_link
  }

  backend {
    balancing_mode = var.backend_balancing_mode
    group          = google_compute_instance_group.default[1].self_link
  }

  backend {
    balancing_mode = var.backend_balancing_mode
    group          = google_compute_instance_group.default[2].self_link
  }

  connection_draining_timeout_sec = var.backend_connection_draining_timeout

  health_checks = local.health_checks
}

resource "google_compute_address" "static-ip-address" {
  for_each = var.nodes

  name = format("%v-%02d-%v-%v-static-ip",
    var.name,
    100 + each.key,
    var.tier,
    var.environment,
  )

  address_type = "INTERNAL"
  address      = cidrhost(var.ip_cidr_range, 100 + each.key)
  subnetwork   = google_compute_subnetwork.subnetwork[0].self_link
}

resource "google_compute_health_check" "tcp" {
  count = var.create_backend_service && var.health_check == "tcp" ? 1 : 0

  name = format("%v-%v-tcp", var.environment, var.name)

  tcp_health_check {
    port = var.health_check_port == 0 ? var.service_port : var.health_check_port
  }
}

resource "google_compute_health_check" "http" {
  count = var.create_backend_service && var.health_check == "http" ? 1 : 0

  name = format("%v-%v-http", var.environment, var.name)

  http_health_check {
    port         = var.health_check_port == 0 ? var.service_port : var.health_check_port
    request_path = var.service_path
  }
}

# Add one instance group per zone
# and only select the appropriate instances
# for each one

resource "google_compute_instance_group" "default" {
  count = length(data.google_compute_zones.available.names)

  name = format("%v-%v-%v",
    var.environment,
    var.name,
    data.google_compute_zones.available.names[count.index],
  )
  description = "Instance group for monitoring VM."
  zone        = data.google_compute_zones.available.names[count.index]

  named_port {
    name = var.name
    port = var.service_port
  }

  named_port {
    name = "http"
    port = "80"
  }

  named_port {
    name = "https"
    port = "443"
  }

  # ignore during migration to google_compute_instance_group_membership
  lifecycle {
    ignore_changes = [instances]
  }
}

resource "google_compute_instance_group_membership" "default" {
  for_each = { for k, v in google_compute_instance.instance_with_attached_disk : k => v }

  instance       = each.value.self_link
  instance_group = google_compute_instance_group.default[index(data.google_compute_zones.available.names, each.value.zone)].name
  zone           = each.value.zone
}

resource "google_compute_disk" "data_disk" {
  # lvm data disks are ready to replace data_disk, at least in testing phases.
  # this change enables us to turn off data_disk so lvm_data_disk can be mounted in its place.
  for_each = { for name, node in var.nodes : name => node if !var.use_data_lvm && !node.use_data_lvm }

  name = format("%v-%02d-%v-%v-data",
    var.name,
    each.key,
    var.tier,
    var.environment,
  )
  project = var.project

  # If we specify a specific zone for the instance, use that...
  # Else if balance_across_zones is set OR if var.zone is blank, then it'll pick the zone based on the ID modulo the number of zones
  # Else use whatever is in var.zone
  zone     = each.value.zone != "" ? each.value.zone : (each.value.balance_across_zones || var.zone == "" ? element(data.google_compute_zones.available.names, each.key) : var.zone)
  size     = var.data_disk_size
  type     = coalesce(each.value.data_disk_type, var.data_disk_type)
  snapshot = var.data_disk_snapshot

  labels = merge(var.labels, {
    environment  = var.environment
    pet_name     = var.name
    do_snapshots = "true"
  })

  timeouts {
    create = var.data_disk_create_timeout
  }

  lifecycle {
    ignore_changes = [snapshot]
  }
}

resource "google_compute_disk" "lvm_data_disk" {
  for_each = {
    for disk in flatten([
      for node, disks in local.lvm_data_disks : [
        for disk_name, disk in disks : merge(disk, { disk_name = disk_name })
      ]
    ]) :
    disk.disk_name => disk
  }

  name = format("%v-%02d-%v-%v-lvm-%d",
    var.name,
    each.value.node_name,
    var.tier,
    var.environment,
    each.value.lvm_disk_idx
  )
  project = var.project

  # If we specify a specific zone for the instance, use that...
  # Else if balance_across_zones is set OR if var.zone is blank, then it'll pick the zone based on the ID modulo the number of zones
  # Else use whatever is in var.zone
  zone     = each.value.zone != "" ? each.value.zone : (each.value.balance_across_zones || var.zone == "" ? element(data.google_compute_zones.available.names, each.value.node_name) : var.zone)
  size     = var.lvm_data_disk_size
  type     = coalesce(each.value.lvm_data_disk_type, var.lvm_data_disk_type)
  snapshot = var.data_disk_snapshot

  labels = merge(var.labels, {
    environment  = var.environment
    pet_name     = var.name
    do_snapshots = "true"
    lvm          = "true"
  })

  timeouts {
    create = var.data_disk_create_timeout
  }

  lifecycle {
    ignore_changes = [snapshot]
  }
}

resource "google_compute_disk" "wal_disk" {
  for_each = { for name, node in var.nodes : name => node if var.use_wal_disk || node.use_wal_disk }
  name = format("%v-%02d-%v-%v-wal",
    var.name,
    each.key,
    var.tier,
    var.environment,
  )
  project = var.project

  # If we specify a specific zone for the instance, use that...
  # Else if balance_across_zones is set OR if var.zone is blank, then it'll pick the zone based on the ID modulo the number of zones
  # Else use whatever is in var.zone
  zone = each.value.zone != "" ? each.value.zone : (each.value.balance_across_zones || var.zone == "" ? element(data.google_compute_zones.available.names, each.key) : var.zone)
  size = each.value.wal_disk_size != null ? each.value.wal_disk_size : var.wal_disk_size
  type = coalesce(each.value.wal_disk_type, var.wal_disk_type)

  labels = merge(var.labels, {
    environment  = var.environment
    pet_name     = var.name
    do_snapshots = "false"
  })

  timeouts {
    create = var.wal_disk_create_timeout
  }

  lifecycle {
    ignore_changes = [snapshot]
  }
}


resource "google_compute_disk" "log_disk" {
  for_each = var.nodes

  name = format("%v-%02d-%v-%v-log",
    var.name,
    each.key,
    var.tier,
    var.environment,
  )
  project = var.project
  zone    = each.value.zone != "" ? each.value.zone : (each.value.balance_across_zones || var.zone == "" ? element(data.google_compute_zones.available.names, each.key) : var.zone)

  size = var.log_disk_size
  type = coalesce(each.value.log_disk_type, var.log_disk_type)

  labels = merge(var.labels, {
    environment = var.environment
  })
}

resource "google_compute_disk" "os_disk_from_snapshot" {
  for_each = local.os_disk_create_nodes

  project  = var.project
  zone     = each.value.zone != "" ? each.value.zone : (each.value.balance_across_zones || var.zone == "" ? element(data.google_compute_zones.available.names, each.key) : var.zone)
  snapshot = each.value.os_disk_snapshot
  name     = format("%v-%02d-%v-%v", var.name, each.key, var.tier, var.environment)
  labels   = var.labels
  type     = coalesce(each.value.os_disk_type, var.os_disk_type)
  lifecycle {
    ignore_changes = [snapshot]
  }
}

resource "google_compute_disk_resource_policy_attachment" "instance_with_attached_disk_os_snap_policy_attachment" {
  count = var.enable_os_disk_snapshots && length(var.nodes) > 0 ? 1 : 0

  project = var.project
  name    = google_compute_resource_policy.os_disk_snapshot_policy[0].name
  disk    = google_compute_instance.instance_with_attached_disk[keys(var.nodes)[0]].name
  zone    = google_compute_instance.instance_with_attached_disk[keys(var.nodes)[0]].zone

  lifecycle {
    replace_triggered_by = [
      google_compute_resource_policy.os_disk_snapshot_policy[0].id
    ]
  }
}

resource "google_compute_instance" "instance_with_attached_disk" {
  for_each = var.nodes

  name    = format("%v-%02d-%v-%v", var.name, each.key, var.tier, var.environment)
  project = var.project
  zone    = each.value.zone != "" ? each.value.zone : (each.value.balance_across_zones || var.zone == "" ? element(data.google_compute_zones.available.names, each.key) : var.zone)

  machine_type              = coalesce(each.value.machine_type, var.machine_type)
  min_cpu_platform          = each.value.min_cpu_platform != "" ? each.value.min_cpu_platform : var.min_cpu_platform
  allow_stopping_for_update = var.allow_stopping_for_update

  metadata = {
    "CHEF_URL"         = var.chef_provision["server_url"]
    "CHEF_VERSION"     = var.chef_provision["version"]
    "CHEF_ENVIRONMENT" = var.environment
    "CHEF_NODE_NAME" = var.use_new_node_name ? format("%v-%02d-%v-%v.c.%v.internal",
      var.name,
      each.key,
      var.tier,
      var.environment,
      var.project,
      ) : format("%v-%02d.%v.%v.%v",
      var.name,
      each.key,
      var.tier,
      var.environment,
      var.dns_zone_name,
    )
    "GL_KERNEL_VERSION"      = var.kernel_version
    "GL_SKIP_KERNEL_INSTALL" = var.skip_kernel_install
    "CHEF_RUN_LIST" = join(",", compact(
      [
        var.chef_run_list,
        each.value.chef_run_list_extra
      ]
    ))
    "CHEF_DNS_ZONE_NAME"           = var.dns_zone_name
    "CHEF_PROJECT"                 = var.project
    "CHEF_BOOTSTRAP_BUCKET"        = var.chef_provision["bootstrap_bucket"]
    "CHEF_BOOTSTRAP_KEYRING"       = var.chef_provision["bootstrap_keyring"]
    "CHEF_BOOTSTRAP_KEY"           = var.chef_provision["bootstrap_key"]
    "CHEF_INIT_RUN_LIST"           = var.chef_init_run_list
    "GL_BOOTSTRAP_DATA_DISK"       = tostring(var.bootstrap_data_disk)
    "GL_BOOTSTRAP_LVM_DATA_DISK"   = tostring(var.bootstrap_lvm_data_disk)
    "GL_PERSISTENT_DISK_PATH"      = var.persistent_disk_path
    "GL_PERSISTENT_LVM_DISK_PATH"  = var.persistent_lvm_disk_path
    "GL_FORMAT_DATA_DISK"          = tostring(var.format_data_disk)
    "GL_FORMAT_LVM_DATA_DISK"      = tostring(var.format_lvm_data_disk)
    "GL_STRIPED_LVM_DATA_DISK"     = tostring(var.lvm_data_disk_stripe)
    "GL_LVM_DATA_DISK_COUNT"       = tostring(var.lvm_data_disk_count)
    "GL_LVM_EXTENT_SIZE"           = tostring(var.lvm_extent_size)
    "GL_INITIAL_BOOT_FORCE_REBOOT" = tostring(var.initial_boot_force_reboot)
    "block-project-ssh-keys"       = upper(tostring(var.block_project_ssh_keys))
    "enable-oslogin"               = upper(tostring(var.enable_oslogin))
    "shutdown-script"              = var.teardown_script != null ? var.teardown_script : module.bootstrap.teardown
    "startup-script"               = var.bootstrap_script != null ? var.bootstrap_script : module.bootstrap.bootstrap
  }

  service_account {
    // this should be the instance under which the instance should be running, rather than the one creating it...
    email = var.service_account_email

    // all the defaults plus cloudkms to access kms
    scopes = [
      "https://www.googleapis.com/auth/cloud.useraccounts.readonly",
      "https://www.googleapis.com/auth/devstorage.read_only",
      "https://www.googleapis.com/auth/logging.write",
      "https://www.googleapis.com/auth/monitoring.write",
      "https://www.googleapis.com/auth/pubsub",
      "https://www.googleapis.com/auth/service.management.readonly",
      "https://www.googleapis.com/auth/servicecontrol",
      "https://www.googleapis.com/auth/trace.append",
      "https://www.googleapis.com/auth/cloudkms",
      "https://www.googleapis.com/auth/compute.readonly",
    ]
  }

  scheduling {
    preemptible = var.preemptible
  }

  boot_disk {
    auto_delete = true
    source      = each.value.os_disk_snapshot != "" ? google_compute_disk.os_disk_from_snapshot[each.key].self_link : null
    dynamic "initialize_params" {
      for_each = each.value.os_disk_snapshot == "" ? [0] : []
      content {
        image  = var.os_boot_image
        size   = var.os_disk_size
        type   = coalesce(each.value.os_disk_type, var.os_disk_type)
        labels = var.labels
      }
    }
  }

  dynamic "attached_disk" {
    for_each = var.use_data_lvm || each.value.use_data_lvm ? [] : [0]
    content {
      source = google_compute_disk.data_disk[each.key].self_link
    }
  }

  attached_disk {
    source      = google_compute_disk.log_disk[each.key].self_link
    device_name = "log"
  }

  dynamic "attached_disk" {
    for_each = each.value.use_wal_disk ? [0] : []
    content {
      source      = google_compute_disk.wal_disk[each.key].self_link
      device_name = "wal"
    }
  }

  dynamic "attached_disk" {
    for_each = local.lvm_data_disks[each.key]
    content {
      source      = google_compute_disk.lvm_data_disk[attached_disk.key].self_link
      device_name = format("lvm-%s", attached_disk.value.lvm_disk_idx)
    }
  }

  network_interface {
    subnetwork = google_compute_subnetwork.subnetwork[0].name
    network_ip = google_compute_address.static-ip-address[each.key].address

    dynamic "access_config" {
      for_each = var.assign_public_ip ? [0] : []
      content {
        nat_ip = var.use_external_ip ? google_compute_address.external[each.key].address : ""
      }
    }
  }

  labels = merge(var.labels, each.value.additional_labels, {
    environment = var.environment
    pet_name    = var.name
  })

  tags = [
    var.name,
    var.environment,
  ]

  lifecycle {
    ignore_changes = [min_cpu_platform, boot_disk, machine_type]
  }
}
