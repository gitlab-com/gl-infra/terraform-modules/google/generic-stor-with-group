locals {
  lvm_data_disks = {
    for node_name, node in var.nodes :
    node_name => {
      for idx in range(0, coalesce(node.lvm_data_disk_count, var.lvm_data_disk_count)) :
      format("%s-lvm-data-%d", node_name, idx) => merge(node, { lvm_disk_idx = idx, node_name = node_name })
      if var.use_data_lvm || node.use_data_lvm
    }
  }
}
