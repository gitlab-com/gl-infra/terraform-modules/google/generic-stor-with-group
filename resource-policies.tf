resource "google_compute_resource_policy" "os_disk_snapshot_policy" {
  count = var.enable_os_disk_snapshots ? 1 : 0

  project = var.project

  name = format(
    "%v-os-snapshot-policy",
    var.name
  )

  snapshot_schedule_policy {
    schedule {
      hourly_schedule {
        hours_in_cycle = var.hours_between_os_disk_snapshots
        start_time     = "00:00"
      }
    }

    snapshot_properties {
      guest_flush = var.enable_os_snapshot_guest_scripts
      labels      = var.labels
    }

    retention_policy {
      max_retention_days    = var.os_disk_snapshot_max_retention_days
      on_source_disk_delete = "APPLY_RETENTION_POLICY"
    }
  }
}
