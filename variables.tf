variable "monitoring_whitelist" {
  type = object({
    subnets = list(string)
    ports   = list(string)
  })

  default = {
    "subnets" = []
    "ports"   = []
  }
}

variable "backend_service_type" {
  type        = string
  description = "Type of backend service, either normal or regional"
  default     = "regular"
}

variable "backend_balancing_mode" {
  type        = string
  description = "Balancing mode of the backend service: `UTILIZATION`, `RATE` or `CONNECTION`"
  default     = "UTILIZATION"
}

variable "backend_connection_draining_timeout" {
  type        = number
  description = "Time (in seconds) for which instance will be drained from the backend service (not accept new connections, but still work to finish started)"
  default     = 300
}

variable "backend_protocol" {
  type    = string
  default = "HTTP"
}

variable "create_backend_service" {
  type    = bool
  default = true
}

variable "data_disk_create_timeout" {
  type        = string
  description = "Timeout applied for creating the data disk. This operation can take a long time if restoring from snapshot."
  default     = "30m"
}

variable "wal_disk_create_timeout" {
  type        = string
  description = "Timeout applied for creating the wal disk. This operation can take a long time if restoring from snapshot."
  default     = "30m"
}

variable "data_disk_snapshot" {
  type        = string
  description = "Restore data disk from a snapshot"

  # An empty string will not use a snapshot for the data disk
  default = ""
}

variable "use_external_ip" {
  type    = bool
  default = false
}

variable "kernel_version" {
  type    = string
  default = ""
}

variable "skip_kernel_install" {
  type        = bool
  description = "Disable upgrading the kernel during bootstraping the instance."
  default     = false
}

variable "use_new_node_name" {
  type    = bool
  default = false
}

variable "allow_stopping_for_update" {
  type        = bool
  description = "Wether Terraform is allowed to stop the instance to update its properties"
  default     = false
}

variable "block_project_ssh_keys" {
  type        = bool
  description = "Whether to block project level SSH keys"
  default     = true
}

variable "persistent_disk_path" {
  type        = string
  description = "default location for disk mount"
  default     = "/var/opt/gitlab"
}

variable "persistent_lvm_disk_path" {
  type        = string
  description = "Default location for disk mount"
  default     = "/var/opt/gitlab-lvm"
}

variable "chef_init_run_list" {
  type        = string
  description = "run_list for the node in chef that are ran on the first boot only"
  default     = ""
}

variable "chef_provision" {
  type        = map(string)
  description = "Configuration details for chef server"
}

variable "chef_run_list" {
  type        = string
  description = "run_list for the node in chef"
}

variable "data_disk_size" {
  type        = number
  description = "The size of the data disk"
  default     = 20
}

variable "wal_disk_size" {
  type        = number
  description = "The size of the wal disk"
  default     = 20
}

variable "lvm_data_disk_size" {
  type        = number
  description = "The size of each LVM PV data disk"
  default     = 20
}

variable "data_disk_type" {
  type        = string
  description = "The type of the data disk"
  default     = "pd-standard"
}

variable "wal_disk_type" {
  type        = string
  description = "The type of the wal disk"
  default     = "pd-standard"
}

variable "lvm_data_disk_type" {
  type        = string
  description = "The type of the LVM data disk"
  default     = "pd-standard"
}

variable "dns_zone_name" {
  type        = string
  description = "The GCP name of the DNS zone to use for this environment"
}

variable "enable_oslogin" {
  type        = bool
  description = "Whether to enable OS Login GCP feature"
  # Note: setting this to TRUE breaks chef!
  # https://gitlab.com/gitlab-com/gitlab-com-infrastructure/merge_requests/297#note_66690562
  default = false
}

variable "environment" {
  type        = string
  description = "The environment name"
}

variable "log_disk_size" {
  type        = number
  description = "The size of the log disk"
  default     = 50
}

variable "log_disk_type" {
  type        = string
  description = "The type of the log disk"
  default     = "pd-standard"
}

variable "bootstrap_data_disk" {
  type        = bool
  description = "Bootstrap script should detect, format, and mount data disk"
  default     = true
}

variable "bootstrap_lvm_data_disk" {
  type        = bool
  description = "Bootstrap script should detect, format, and mount LVM data disk"
  default     = false
}

variable "format_data_disk" {
  type        = bool
  description = "Force formatting of the persistent disk."
  default     = false
}

variable "format_lvm_data_disk" {
  type        = bool
  description = "Force formatting of the LVM-based persistent disk"
  default     = false
}

variable "use_wal_disk" {
  type        = bool
  default     = false
  description = "If true, a dedicated wal disk will be attached to all nodes of the cluster"
}

variable "use_data_lvm" {
  type        = bool
  default     = false
  description = "If true, an additional data disk will be created as an LVM LV, possibly across multiple PVs"
}

variable "lvm_data_disk_stripe" {
  type        = bool
  default     = false
  description = "If true, LVM striping will be used for the LVM data disk"
}

variable "lvm_data_disk_count" {
  type        = number
  default     = 4
  description = "The number of LVM PVs to assign to the VG/LV"
}

variable "lvm_extent_size" {
  type        = string
  default     = "4M"
  description = "The Physical Extent Size for the LVM VG"
}

variable "initial_boot_force_reboot" {
  type        = bool
  description = "Force an initail reboot after bootstrapping"
  default     = false
}
variable "health_check" {
  type    = string
  default = "http"

  validation {
    condition     = contains(["http", "tcp"], var.health_check)
    error_message = "The health_check value must be \"http\" or \"tcp\"."
  }
}

variable "health_check_port" {
  type        = number
  description = "Health check port, if 0 use `service_port` (default)"
  default     = 0
}

variable "ip_cidr_range" {
  type        = string
  description = "The IP range"
}

variable "machine_type" {
  type        = string
  description = "The machine size"
}

variable "min_cpu_platform" {
  type        = string
  description = "CPU platform description"
  default     = null
}

variable "name" {
  type        = string
  description = "The pet name"
}

variable "os_boot_image" {
  type        = string
  description = "The OS image to boot"
  default     = "ubuntu-os-cloud/ubuntu-2004-lts"
}

variable "os_disk_size" {
  type        = number
  description = "The OS disk size in GiB"
  default     = 20
}

variable "os_disk_type" {
  type        = string
  description = "The OS disk type"
  default     = "pd-standard"
}

variable "preemptible" {
  type        = bool
  description = "Use preemptible instances for this pet"
  default     = false
}

variable "project" {
  type        = string
  description = "The project name"
}

variable "public_ports" {
  type        = list(string)
  description = "The list of ports that should be publicly reachable"
  default     = []
}

variable "region" {
  type        = string
  description = "The target region"
}

variable "service_account_email" {
  type        = string
  description = "Service account emails under which the instance is running"
}

variable "service_path" {
  type    = string
  default = "/"
}

variable "service_port" {
  type    = number
  default = 80
}

variable "bootstrap_script" {
  type        = string
  description = "user-provided bootstrap script to override the bootstrap module"
  default     = null
}

variable "teardown_script" {
  type        = string
  description = "user-provided teardown script to override the bootstrap module"
  default     = null
}

variable "tier" {
  type        = string
  description = "The tier for this service"
}

variable "vpc" {
  type        = string
  description = "The target network"
}

variable "zone" {
  type    = string
  default = ""
}

# Instances without public IPs cannot access the public internet without NAT.
# Ensure that a Cloud NAT instance covers the subnetwork/region for this
# instance.
variable "assign_public_ip" {
  type    = bool
  default = true
}

variable "nodes" {
  type = map(object({
    chef_run_list_extra  = optional(string, "")
    zone                 = optional(string, "")
    balance_across_zones = optional(bool, false)
    machine_type         = optional(string, "")
    min_cpu_platform     = optional(string, "")
    os_disk_type         = optional(string, "")
    log_disk_type        = optional(string, "")
    data_disk_type       = optional(string, "")
    lvm_data_disk_type   = optional(string, "")
    os_disk_snapshot     = optional(string, "")
    wal_disk_type        = optional(string)
    wal_disk_size        = optional(number)
    use_wal_disk         = optional(bool, false)
    use_data_lvm         = optional(bool, false)
    lvm_data_disk_stripe = optional(bool, false)
    lvm_data_disk_size   = optional(number)
    lvm_data_disk_count  = optional(number)
    lvm_extent_size      = optional(string, "4M")
    additional_labels    = optional(map(string), {})
  }))

  default = {}

  validation {
    condition = alltrue([
      for id, node in var.nodes : can(regex("^\\d+$", id))
    ])
    error_message = "All nodes must have a numeric key."
  }
}

# Resource labels
variable "labels" {
  type        = map(string)
  description = "Labels to add to each of the managed resources."
  default     = {}
}

# OS disk snapshot variables
variable "enable_os_disk_snapshots" {
  type        = bool
  description = "Enable creation of OS disk snapshot resource and attachment policies"
  default     = false
}

variable "os_disk_snapshot_max_retention_days" {
  type        = number
  description = "Number of days to retain snapshots, defaults to 3."
  default     = 3
}

variable "enable_os_snapshot_guest_scripts" {
  type    = bool
  default = true
}

variable "hours_between_os_disk_snapshots" {
  type        = number
  description = "Setting this will enable hourly os disk snapshots, 0 to disable"
  default     = 0
}
