output "instances_self_link" {
  value = [for instance in google_compute_instance.instance_with_attached_disk : instance.self_link]
}

output "instance_groups_self_link" {
  value = google_compute_instance_group.default[*].self_link
}

output "http_health_check_self_link" {
  value = var.create_backend_service && var.health_check == "http" ? google_compute_health_check.http[0].self_link : ""
}

output "tcp_health_check_self_link" {
  value = var.create_backend_service && var.health_check == "tcp" ? google_compute_health_check.tcp[0].self_link : ""
}

# this is idiomatic for how to have outputs that may have a count of zero
# https://www.terraform.io/upgrade-guides/0-11.html#referencing-attributes-from-resources-with-count-0
output "google_compute_backend_service_self_link" {
  value = element(
    concat(google_compute_backend_service.default[*].self_link, [""]),
    0,
  )
}

output "google_compute_region_backend_service_self_link" {
  value = element(
    concat(
      google_compute_region_backend_service.default[*].self_link,
      [""],
    ),
    0,
  )
}

output "google_compute_subnetwork_name" {
  value = element(concat(google_compute_subnetwork.subnetwork[*].name, [""]), 0)
}

output "google_compute_subnetwork_self_link" {
  value = element(
    concat(google_compute_subnetwork.subnetwork[*].self_link, [""]),
    0,
  )
}

output "google_compute_address_static_ips" {
  value = [for ip in google_compute_address.static-ip-address : ip.address]
}
