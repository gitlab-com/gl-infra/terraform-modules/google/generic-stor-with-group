resource "google_compute_subnetwork" "subnetwork" {
  count = length(var.nodes) > 0 ? 1 : 0

  name                     = format("%v-%v", var.name, var.environment)
  project                  = var.project
  region                   = var.region
  network                  = var.vpc
  ip_cidr_range            = var.ip_cidr_range
  private_ip_google_access = true
}
