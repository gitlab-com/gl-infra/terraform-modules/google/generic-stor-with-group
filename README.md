# GitLab.com Generic Storage with Group Terraform Module

## What is this?

This module provisions generic storage GCE instance groups.

## What is Terraform?

[Terraform](https://www.terraform.io) is an infrastructure-as-code tool that greatly reduces the amount of time needed to implement and scale our infrastructure. It's provider agnostic so it works great for our use case. You're encouraged to read the documentation as it's very well written.

<!-- BEGIN_TF_DOCS -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 1.3 |
| <a name="requirement_google"></a> [google](#requirement\_google) | >= 4.0.0 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_google"></a> [google](#provider\_google) | >= 4.0.0 |

## Modules

| Name | Source | Version |
|------|--------|---------|
| <a name="module_bootstrap"></a> [bootstrap](#module\_bootstrap) | ops.gitlab.net/gitlab-com/bootstrap/google | 5.5.8 |

## Resources

| Name | Type |
|------|------|
| [google_compute_address.external](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_address) | resource |
| [google_compute_address.static-ip-address](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_address) | resource |
| [google_compute_backend_service.default](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_backend_service) | resource |
| [google_compute_disk.data_disk](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_disk) | resource |
| [google_compute_disk.log_disk](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_disk) | resource |
| [google_compute_disk.lvm_data_disk](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_disk) | resource |
| [google_compute_disk.os_disk_from_snapshot](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_disk) | resource |
| [google_compute_disk.wal_disk](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_disk) | resource |
| [google_compute_disk_resource_policy_attachment.instance_with_attached_disk_os_snap_policy_attachment](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_disk_resource_policy_attachment) | resource |
| [google_compute_firewall.monitoring_whitelist](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_firewall) | resource |
| [google_compute_firewall.public](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_firewall) | resource |
| [google_compute_health_check.http](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_health_check) | resource |
| [google_compute_health_check.tcp](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_health_check) | resource |
| [google_compute_instance.instance_with_attached_disk](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_instance) | resource |
| [google_compute_instance_group.default](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_instance_group) | resource |
| [google_compute_instance_group_membership.default](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_instance_group_membership) | resource |
| [google_compute_region_backend_service.default](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_region_backend_service) | resource |
| [google_compute_resource_policy.os_disk_snapshot_policy](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_resource_policy) | resource |
| [google_compute_subnetwork.subnetwork](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_subnetwork) | resource |
| [google_compute_zones.available](https://registry.terraform.io/providers/hashicorp/google/latest/docs/data-sources/compute_zones) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_allow_stopping_for_update"></a> [allow\_stopping\_for\_update](#input\_allow\_stopping\_for\_update) | Wether Terraform is allowed to stop the instance to update its properties | `bool` | `false` | no |
| <a name="input_assign_public_ip"></a> [assign\_public\_ip](#input\_assign\_public\_ip) | Instances without public IPs cannot access the public internet without NAT. Ensure that a Cloud NAT instance covers the subnetwork/region for this instance. | `bool` | `true` | no |
| <a name="input_backend_balancing_mode"></a> [backend\_balancing\_mode](#input\_backend\_balancing\_mode) | Balancing mode of the backend service: `UTILIZATION`, `RATE` or `CONNECTION` | `string` | `"UTILIZATION"` | no |
| <a name="input_backend_connection_draining_timeout"></a> [backend\_connection\_draining\_timeout](#input\_backend\_connection\_draining\_timeout) | Time (in seconds) for which instance will be drained from the backend service (not accept new connections, but still work to finish started) | `number` | `300` | no |
| <a name="input_backend_protocol"></a> [backend\_protocol](#input\_backend\_protocol) | n/a | `string` | `"HTTP"` | no |
| <a name="input_backend_service_type"></a> [backend\_service\_type](#input\_backend\_service\_type) | Type of backend service, either normal or regional | `string` | `"regular"` | no |
| <a name="input_block_project_ssh_keys"></a> [block\_project\_ssh\_keys](#input\_block\_project\_ssh\_keys) | Whether to block project level SSH keys | `bool` | `true` | no |
| <a name="input_bootstrap_data_disk"></a> [bootstrap\_data\_disk](#input\_bootstrap\_data\_disk) | Bootstrap script should detect, format, and mount data disk | `bool` | `true` | no |
| <a name="input_bootstrap_lvm_data_disk"></a> [bootstrap\_lvm\_data\_disk](#input\_bootstrap\_lvm\_data\_disk) | Bootstrap script should detect, format, and mount LVM data disk | `bool` | `false` | no |
| <a name="input_bootstrap_script"></a> [bootstrap\_script](#input\_bootstrap\_script) | user-provided bootstrap script to override the bootstrap module | `string` | `null` | no |
| <a name="input_chef_init_run_list"></a> [chef\_init\_run\_list](#input\_chef\_init\_run\_list) | run\_list for the node in chef that are ran on the first boot only | `string` | `""` | no |
| <a name="input_chef_provision"></a> [chef\_provision](#input\_chef\_provision) | Configuration details for chef server | `map(string)` | n/a | yes |
| <a name="input_chef_run_list"></a> [chef\_run\_list](#input\_chef\_run\_list) | run\_list for the node in chef | `string` | n/a | yes |
| <a name="input_create_backend_service"></a> [create\_backend\_service](#input\_create\_backend\_service) | n/a | `bool` | `true` | no |
| <a name="input_data_disk_create_timeout"></a> [data\_disk\_create\_timeout](#input\_data\_disk\_create\_timeout) | Timeout applied for creating the data disk. This operation can take a long time if restoring from snapshot. | `string` | `"30m"` | no |
| <a name="input_data_disk_size"></a> [data\_disk\_size](#input\_data\_disk\_size) | The size of the data disk | `number` | `20` | no |
| <a name="input_data_disk_snapshot"></a> [data\_disk\_snapshot](#input\_data\_disk\_snapshot) | Restore data disk from a snapshot | `string` | `""` | no |
| <a name="input_data_disk_type"></a> [data\_disk\_type](#input\_data\_disk\_type) | The type of the data disk | `string` | `"pd-standard"` | no |
| <a name="input_dns_zone_name"></a> [dns\_zone\_name](#input\_dns\_zone\_name) | The GCP name of the DNS zone to use for this environment | `string` | n/a | yes |
| <a name="input_enable_os_disk_snapshots"></a> [enable\_os\_disk\_snapshots](#input\_enable\_os\_disk\_snapshots) | Enable creation of OS disk snapshot resource and attachment policies | `bool` | `false` | no |
| <a name="input_enable_os_snapshot_guest_scripts"></a> [enable\_os\_snapshot\_guest\_scripts](#input\_enable\_os\_snapshot\_guest\_scripts) | n/a | `bool` | `true` | no |
| <a name="input_enable_oslogin"></a> [enable\_oslogin](#input\_enable\_oslogin) | Whether to enable OS Login GCP feature | `bool` | `false` | no |
| <a name="input_environment"></a> [environment](#input\_environment) | The environment name | `string` | n/a | yes |
| <a name="input_format_data_disk"></a> [format\_data\_disk](#input\_format\_data\_disk) | Force formatting of the persistent disk. | `bool` | `false` | no |
| <a name="input_format_lvm_data_disk"></a> [format\_lvm\_data\_disk](#input\_format\_lvm\_data\_disk) | Force formatting of the LVM-based persistent disk | `bool` | `false` | no |
| <a name="input_health_check"></a> [health\_check](#input\_health\_check) | n/a | `string` | `"http"` | no |
| <a name="input_health_check_port"></a> [health\_check\_port](#input\_health\_check\_port) | Health check port, if 0 use `service_port` (default) | `number` | `0` | no |
| <a name="input_hours_between_os_disk_snapshots"></a> [hours\_between\_os\_disk\_snapshots](#input\_hours\_between\_os\_disk\_snapshots) | Setting this will enable hourly os disk snapshots, 0 to disable | `number` | `0` | no |
| <a name="input_initial_boot_force_reboot"></a> [initial\_boot\_force\_reboot](#input\_initial\_boot\_force\_reboot) | Force an initail reboot after bootstrapping | `bool` | `false` | no |
| <a name="input_ip_cidr_range"></a> [ip\_cidr\_range](#input\_ip\_cidr\_range) | The IP range | `string` | n/a | yes |
| <a name="input_kernel_version"></a> [kernel\_version](#input\_kernel\_version) | n/a | `string` | `""` | no |
| <a name="input_labels"></a> [labels](#input\_labels) | Labels to add to each of the managed resources. | `map(string)` | `{}` | no |
| <a name="input_log_disk_size"></a> [log\_disk\_size](#input\_log\_disk\_size) | The size of the log disk | `number` | `50` | no |
| <a name="input_log_disk_type"></a> [log\_disk\_type](#input\_log\_disk\_type) | The type of the log disk | `string` | `"pd-standard"` | no |
| <a name="input_lvm_data_disk_count"></a> [lvm\_data\_disk\_count](#input\_lvm\_data\_disk\_count) | The number of LVM PVs to assign to the VG/LV | `number` | `4` | no |
| <a name="input_lvm_data_disk_size"></a> [lvm\_data\_disk\_size](#input\_lvm\_data\_disk\_size) | The size of each LVM PV data disk | `number` | `20` | no |
| <a name="input_lvm_data_disk_stripe"></a> [lvm\_data\_disk\_stripe](#input\_lvm\_data\_disk\_stripe) | If true, LVM striping will be used for the LVM data disk | `bool` | `false` | no |
| <a name="input_lvm_data_disk_type"></a> [lvm\_data\_disk\_type](#input\_lvm\_data\_disk\_type) | The type of the LVM data disk | `string` | `"pd-standard"` | no |
| <a name="input_lvm_extent_size"></a> [lvm\_extent\_size](#input\_lvm\_extent\_size) | The Physical Extent Size for the LVM VG | `string` | `"4M"` | no |
| <a name="input_machine_type"></a> [machine\_type](#input\_machine\_type) | The machine size | `string` | n/a | yes |
| <a name="input_min_cpu_platform"></a> [min\_cpu\_platform](#input\_min\_cpu\_platform) | CPU platform description | `string` | `null` | no |
| <a name="input_monitoring_whitelist"></a> [monitoring\_whitelist](#input\_monitoring\_whitelist) | n/a | <pre>object({<br/>    subnets = list(string)<br/>    ports   = list(string)<br/>  })</pre> | <pre>{<br/>  "ports": [],<br/>  "subnets": []<br/>}</pre> | no |
| <a name="input_name"></a> [name](#input\_name) | The pet name | `string` | n/a | yes |
| <a name="input_nodes"></a> [nodes](#input\_nodes) | n/a | <pre>map(object({<br/>    chef_run_list_extra  = optional(string, "")<br/>    zone                 = optional(string, "")<br/>    balance_across_zones = optional(bool, false)<br/>    machine_type         = optional(string, "")<br/>    min_cpu_platform     = optional(string, "")<br/>    os_disk_type         = optional(string, "")<br/>    log_disk_type        = optional(string, "")<br/>    data_disk_type       = optional(string, "")<br/>    lvm_data_disk_type   = optional(string, "")<br/>    os_disk_snapshot     = optional(string, "")<br/>    wal_disk_type        = optional(string)<br/>    wal_disk_size        = optional(number)<br/>    use_wal_disk         = optional(bool, false)<br/>    use_data_lvm         = optional(bool, false)<br/>    lvm_data_disk_stripe = optional(bool, false)<br/>    lvm_data_disk_size   = optional(number)<br/>    lvm_data_disk_count  = optional(number)<br/>    lvm_extent_size      = optional(string, "4M")<br/>    additional_labels    = optional(map(string), {})<br/>  }))</pre> | `{}` | no |
| <a name="input_os_boot_image"></a> [os\_boot\_image](#input\_os\_boot\_image) | The OS image to boot | `string` | `"ubuntu-os-cloud/ubuntu-2004-lts"` | no |
| <a name="input_os_disk_size"></a> [os\_disk\_size](#input\_os\_disk\_size) | The OS disk size in GiB | `number` | `20` | no |
| <a name="input_os_disk_snapshot_max_retention_days"></a> [os\_disk\_snapshot\_max\_retention\_days](#input\_os\_disk\_snapshot\_max\_retention\_days) | Number of days to retain snapshots, defaults to 3. | `number` | `3` | no |
| <a name="input_os_disk_type"></a> [os\_disk\_type](#input\_os\_disk\_type) | The OS disk type | `string` | `"pd-standard"` | no |
| <a name="input_persistent_disk_path"></a> [persistent\_disk\_path](#input\_persistent\_disk\_path) | default location for disk mount | `string` | `"/var/opt/gitlab"` | no |
| <a name="input_persistent_lvm_disk_path"></a> [persistent\_lvm\_disk\_path](#input\_persistent\_lvm\_disk\_path) | Default location for disk mount | `string` | `"/var/opt/gitlab-lvm"` | no |
| <a name="input_preemptible"></a> [preemptible](#input\_preemptible) | Use preemptible instances for this pet | `bool` | `false` | no |
| <a name="input_project"></a> [project](#input\_project) | The project name | `string` | n/a | yes |
| <a name="input_public_ports"></a> [public\_ports](#input\_public\_ports) | The list of ports that should be publicly reachable | `list(string)` | `[]` | no |
| <a name="input_region"></a> [region](#input\_region) | The target region | `string` | n/a | yes |
| <a name="input_service_account_email"></a> [service\_account\_email](#input\_service\_account\_email) | Service account emails under which the instance is running | `string` | n/a | yes |
| <a name="input_service_path"></a> [service\_path](#input\_service\_path) | n/a | `string` | `"/"` | no |
| <a name="input_service_port"></a> [service\_port](#input\_service\_port) | n/a | `number` | `80` | no |
| <a name="input_skip_kernel_install"></a> [skip\_kernel\_install](#input\_skip\_kernel\_install) | Disable upgrading the kernel during bootstraping the instance. | `bool` | `false` | no |
| <a name="input_teardown_script"></a> [teardown\_script](#input\_teardown\_script) | user-provided teardown script to override the bootstrap module | `string` | `null` | no |
| <a name="input_tier"></a> [tier](#input\_tier) | The tier for this service | `string` | n/a | yes |
| <a name="input_use_data_lvm"></a> [use\_data\_lvm](#input\_use\_data\_lvm) | If true, an additional data disk will be created as an LVM LV, possibly across multiple PVs | `bool` | `false` | no |
| <a name="input_use_external_ip"></a> [use\_external\_ip](#input\_use\_external\_ip) | n/a | `bool` | `false` | no |
| <a name="input_use_new_node_name"></a> [use\_new\_node\_name](#input\_use\_new\_node\_name) | n/a | `bool` | `false` | no |
| <a name="input_use_wal_disk"></a> [use\_wal\_disk](#input\_use\_wal\_disk) | If true, a dedicated wal disk will be attached to all nodes of the cluster | `bool` | `false` | no |
| <a name="input_vpc"></a> [vpc](#input\_vpc) | The target network | `string` | n/a | yes |
| <a name="input_wal_disk_create_timeout"></a> [wal\_disk\_create\_timeout](#input\_wal\_disk\_create\_timeout) | Timeout applied for creating the wal disk. This operation can take a long time if restoring from snapshot. | `string` | `"30m"` | no |
| <a name="input_wal_disk_size"></a> [wal\_disk\_size](#input\_wal\_disk\_size) | The size of the wal disk | `number` | `20` | no |
| <a name="input_wal_disk_type"></a> [wal\_disk\_type](#input\_wal\_disk\_type) | The type of the wal disk | `string` | `"pd-standard"` | no |
| <a name="input_zone"></a> [zone](#input\_zone) | n/a | `string` | `""` | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_google_compute_address_static_ips"></a> [google\_compute\_address\_static\_ips](#output\_google\_compute\_address\_static\_ips) | n/a |
| <a name="output_google_compute_backend_service_self_link"></a> [google\_compute\_backend\_service\_self\_link](#output\_google\_compute\_backend\_service\_self\_link) | this is idiomatic for how to have outputs that may have a count of zero https://www.terraform.io/upgrade-guides/0-11.html#referencing-attributes-from-resources-with-count-0 |
| <a name="output_google_compute_region_backend_service_self_link"></a> [google\_compute\_region\_backend\_service\_self\_link](#output\_google\_compute\_region\_backend\_service\_self\_link) | n/a |
| <a name="output_google_compute_subnetwork_name"></a> [google\_compute\_subnetwork\_name](#output\_google\_compute\_subnetwork\_name) | n/a |
| <a name="output_google_compute_subnetwork_self_link"></a> [google\_compute\_subnetwork\_self\_link](#output\_google\_compute\_subnetwork\_self\_link) | n/a |
| <a name="output_http_health_check_self_link"></a> [http\_health\_check\_self\_link](#output\_http\_health\_check\_self\_link) | n/a |
| <a name="output_instance_groups_self_link"></a> [instance\_groups\_self\_link](#output\_instance\_groups\_self\_link) | n/a |
| <a name="output_instances_self_link"></a> [instances\_self\_link](#output\_instances\_self\_link) | n/a |
| <a name="output_tcp_health_check_self_link"></a> [tcp\_health\_check\_self\_link](#output\_tcp\_health\_check\_self\_link) | n/a |
<!-- END_TF_DOCS -->

## Contributing

Please see [CONTRIBUTING.md](./CONTRIBUTING.md).

## License

See [LICENSE](./LICENSE).
